# mule3.watermarking

## 1. Introduction
This (very) small lib provides a better watermarking support for mule v3.7.0 (and higher) projects.
In mule v3.x watermarking is supported for the poll connector only - and only for "batch-processing".
This was solved in mule v4 by separating watermarking into its own component.
However, this lib introduces a much better controllable watermarking for mule v3.x projects.
Furthermore, like the mule v4 watermarking it also supports other work models like http-triggered flows.

## 2. Usage
This project provides two sub-flows that are used to retrieve and store the watermark.
  * retrieveLastWatermarkFlow
  * storeNewWatermarkFlow

A watermark can be anything: a simple sequence number, a timestamp or a complex object.

The implementation uses outbound properties to not temper with the payload.
The watermark is saved in the default persistent object store under a customizable key(see Setup section for details).

### retreiveLastWatermarkFlow
Use this sub-flow to retrieve the last saved watermark. Simply add a flow-ref component:

    <flow-ref name="retrieveLastWatermarkFlow" doc:name="Retrieve Watermark"/>

The watermark is retrieved from the objectstore using the customized key (see Setup section for details) and is stored in the outbound property `watermark`.
Now the user can access the watermark and use it in whatever request of his.

### storeNewWatermarkFlow
This sub-flow is used to store the last watermark. Again, simply use a flow-ref component:

    <flow-ref name="storeNewWatermarkFlow" doc:name="Retrieve Watermark"/>

The flow uses a dataweave script to compute the watermark of an object and store it in the objectstore. To setup the script, see the setup section below.

## 3. Setup

#### Add Maven Dependency
First of all, add the following dependency to your project pom:

	<dependency>
		<groupdId>cc.renken</groupId>
		<artifactId>mule3.watermarking</artifactId>
		<version>1.0.0</version>
	</dependency>
	
#### Load Mule Configuration (Flows)
Furthermore, in your mule configuration file you'll need to load the mule configuration file that contains the sub-flows to make them available within your project.
Therefore, you need to add:

```xml
<spring:beans>
   <spring:import resource="classpath:cc.renken.mule3.watermarking.xml"/>
</spring:beans>
```

#### Set Objectstore Key
Furthermore, you need to provide a value for the property `cc.renken.mule3.watermark.key`.
This key is then used to retrieve and store the watermark in the objectstore.
You can set an arbitrary value.
However, the project name is most strongly recommended.
This will allow the user to store different watermarks for different projects running within the same mule runtime.

#### Default Watermark Script
When the mule application runs for the first time.
Then no watermark was stored yet.
Then, a default value is "computed", stored and returned as the current watermark.
The default watermark is computed by a dataweave script the user MUST provide.
The script must be located on the classpath such that mule can find and load it.
The script has to be named `defaultWatermark.dwl`.
If the objectstore does not contain a watermark yet, a default value must be computed/provided.
To allow the user full customization of the watermark, the library uses a "pre-specified" script called `defaultWatermark.dwl`.
However, obviously, the library does not contain this file, instead the user has to provide it.

Simply create a new text file in Anypoint Studio and locate it on the classpath.
The default location for dataweave scripts is the folder `/mappings`.
The script simply return the watermark and nothing else.
However, it might use the payload of the incoming message to compute the value.

The following example uses the current timestamp as the default watermark:

    %dw 1.0
    %output application/java
    ---
    now

The second example simply starts with the sequence number zero.

    %dw 1.0
    %output application/java
    ---
    0

### storeNewWatermarkFlow
This sub-flow is used to store the watermark.
It can be called at any time anywhere within the flow.
It loads and executes the script `newWatermark.dwl` to determine the new watermark.
The script will get any message that is send to the sub-flow.
Like `defaultWatermark.dwl` it must be located on the root of the classpath to be found by the sub-flow.

Again, simply create a new text file in Anypoint Studio and locate it on the classpath.
The default location for dataweave scripts is the folder `/mappings`.

The script MUST return the watermark and nothing else.
However, it might use the payload of the incoming message to compute the value.

The following example returns the current timestamp as the new watermark:

    %dw 1.0
    %output application/java
    ---
    now

The second example simply returns the property `id` of the payload as the new watermark.

    %dw 1.0
    %output application/java
    ---
    payload.id


[1] https://docs.mulesoft.com/mule-runtime/4.1/migration-patterns-watermark
